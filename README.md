# Projet final Java EE Spring prêt à l'emploi

## Présentation

C'est un projet maven standard, avec l'organisation classique.
Le projet utilise Spring classique et Spring MVC, Bootstrap 4, Postgresql.
Il a été modifié pour être déployé sur la plateforme [Heroku](https://mooc-drive.herokuapp.com) .

## Utilisation

L'application est un site marchand simplifié.
On peut soit se connecter puis commencer à remplir le panier.
On peut aussi remplir son panier en tant que visiteur anonyme, et s'authentifier au moment de commander.
A ce moment, le panier récupère le contenu du dernier panier actif du client et on y ajoute les articles du panier anonyme.
Un utilisateur authentifié peut récupérer l'état de son panier, consulter ses commandes et accéder au contenu de ses commandes.

## Gestion des stocks disponibles

Il y a une gestion simple du stock (quantité disponible) : lors de l'initialisation de la base de données, chaque article est initialisé avec une valeur de stock comprise entre 0 et 9.
Le nombre d'articles en stock est rappelé sur chaque article. C'est pour faciliter le diagnostic.
Seuls les articles disponibles sont présentés dans les rayons.
La gestion est plutôt simple : Le stock courant est utilisé au moment valider la commande : les articles en trop sont retirés et le client est prévenu.

Pour faciliter la vie, pas besoin de créer des comptes utilisateur : il y a en a déjà 3 prédéfinis.
Leurs email / mot de passe sont :

-   Customer A : a@a / a
-   Customer B : b@b / b
-   Customer C : c@c / c

## Technique

Tomcat embarqué via webapp-launcher, fourni par Heroku, très similaire à une architecture Spring boot.
Base de données Postgresql initialisée et migrée via Flyway.
Couche de présentation : Bootstrap 4 sans personnalisation pour avoir une maquette acceptable et responsive, Fontawesome.
Aucun framework frontend pour l'ajax : le minimum en jquery et javascript.

## Packaging et déployment, aide-mémoire

### Prérequis

Le pom.xml est configuré pour java 11.
Maven wrapper dispense d'installer une version spécifique de maven.
Avoir une base de données postgresql qui tourne, même vide.

### En local, ligne de commande

Avant d'initialiser la base de données avec le plugin Flyway, il faut configurer les variables d'environnement :

```bash
export FLYWAY_URL=jdbc:postgresql://localhost:5432/<databasename>
export FLYWAY_USER=<user>
export FLYWAY_PASSWORD=<password>
```

Initialiser la base de données locale

```bash
./mvnw flyway:migrate
```

Vider la base de données locale

```bash
./mvnw flyway:clean
```

Compiler et installer l'application

```bash
./mvnw clean install
```

Démarrer l'application

```bash
java -jar target/dependency/webapp-runner.jar target/drive-0.0.1.war
```

L'application est disponible à l'URL http://localhost:8080/catalog/categories.html

### En local avec Eclipse

Au préalable, il faut toujours initialiser la base de données locale.
Créer une configuration de type Java Application
Main class=webapp.runner.launch.Main
Program arguments=./src/main/webapp
VM arguments=-Dtomcat.util.scan.StandardJarScanFilter.jarsToSkip=txw\*,jaxb\*
Environment variables to set:
DATABASE_URL=postgres://<user>:<password>@localhost:5432/<databasename>

L'application est disponible à l'URL http://localhost:8080/catalog/categories.html
Les modifications de code sont prises à chaud.

### Heroku en local

Il faut créer un fichier .env utilisé par Heroku pour fournir les variables d'environnement :
DATABASE_URL=postgres://<user>:<password>@localhost:5432/<databasename>

Il faut compiler et installer l'application en ligne de commande.
Si nécessaire, exécuter les migrations :

```bash
heroku local release
```

Démarrer l'application

```bash
heroku local web
```

L'application est disponible à l'URL http://localhost:5000/catalog/categories.html

### Heroku hebergé

Une fois que les modifications sont commitées.

```bash
git push heroku master
```

où heroku est le nom de la remote configuré sur git, master est la branche à déployer.
C'est le fichier Procfile qui indique la ligne de commande à exécuter, et la target release provoque la migration au moment du déploiement.
