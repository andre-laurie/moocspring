package fr.eservices.drive.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "fr.eservices.drive.service")
public class ServiceConfig {

}
