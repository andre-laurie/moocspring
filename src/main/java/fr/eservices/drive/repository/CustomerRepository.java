package fr.eservices.drive.repository;

import org.springframework.data.repository.CrudRepository;

import fr.eservices.drive.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {

	Customer findCustomerByEmailAndPassword(String email, String encoded);

}
