package fr.eservices.drive.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.eservices.drive.model.Article;
import fr.eservices.drive.model.Category;

public interface ArticleRepository extends CrudRepository<Article, Integer> {

	List<Article> findByCategoriesAndStockGreaterThanOrderByNameAsc(Category category, int stock);

}
