package fr.eservices.drive.repository;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import fr.eservices.drive.model.Customer;
import fr.eservices.drive.model.Order;

public interface OrderRepository extends CrudRepository<Order, Integer> {

	List<Order> findByCustomerOrderByCreatedOnDesc(Customer customer);

	@Override
	@EntityGraph(attributePaths = "articles")
	Order findOne(Integer id);

}
