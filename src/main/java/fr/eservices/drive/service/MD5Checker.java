package fr.eservices.drive.service;

import java.lang.invoke.MethodHandles;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class MD5Checker implements PasswordChecker {

	private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	@Override
	public String encode(String email, String password) {

		LOGGER.debug("encode: login={}", email);

		MessageDigest messageDigest;
		try {
			messageDigest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException("Impossible de créer le MessageDigest");
		}
		byte[] digest = messageDigest.digest((email + password).getBytes());
		byte[] encode = Base64.getEncoder().encode(digest);
		return new String(encode);
	}

}
