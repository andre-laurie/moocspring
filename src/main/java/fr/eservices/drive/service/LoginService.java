package fr.eservices.drive.service;

import java.lang.invoke.MethodHandles;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.eservices.drive.model.Article;
import fr.eservices.drive.model.Cart;
import fr.eservices.drive.model.Customer;
import fr.eservices.drive.model.Order;
import fr.eservices.drive.model.Status;
import fr.eservices.drive.model.StatusHistory;
import fr.eservices.drive.repository.CartRepository;
import fr.eservices.drive.repository.CustomerRepository;
import fr.eservices.drive.repository.OrderRepository;
import fr.eservices.drive.repository.StatusHistoryRepository;
import fr.eservices.drive.web.dto.CartPojo;

@Service
@Transactional
public class LoginService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private CartRepository cartRepository;

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private StatusHistoryRepository statusHistoryRepository;

	@Autowired
	private PasswordChecker passwordChecker;

	public Customer login(String email, String password, CartPojo anonymousCart) {

		String encoded = passwordChecker.encode(email, password);

		var customer = customerRepository.findCustomerByEmailAndPassword(email, encoded);
		if (customer == null) {
			return null;
		}
		var activeCart = customer.getActiveCart();
		if (activeCart == null) {
			LOGGER.info("Merging cart; no current activeCart, anonymousCart becomes activeCart");
			var newCart = anonymousCart.toCart();
			newCart.setCustomer(customer);
			newCart = cartRepository.save(newCart);
			customer.setActiveCart(newCart);
		} else {
			LOGGER.info("Merging cart; anonymousCart articles are added to activeCart");
			activeCart.getArticles().addAll(anonymousCart.getArticles());
		}
		return customer;
	}

	public List<Article> validateOrder(int customerId) {

		var customer = customerRepository.findOne(customerId);
		var cart = customer.getActiveCart();
		List<Article> articles = cart.getArticles();
		LOGGER.info("Validating order: customer.id={}, nb articles={}", customerId, articles.size());

		// Check article count in cart vs availability in stock:
		List<Article> missingArticles = new ArrayList<>();
		Map<Article, Long> articlesCountInCart = articles.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		for (Entry<Article, Long> e : articlesCountInCart.entrySet()) {
			var article = e.getKey();
			var countInCart = e.getValue().intValue();
			int countInStock = article.getStock();
			LOGGER.info("Validating order: Article count: name={}, nb in cart={}, nb in stock={}", article.getName(), countInCart, countInStock);

			if (countInCart > countInStock) {
				missingArticles.add(article);
				int excess = countInCart - countInStock;
				LOGGER.info("Validating order: Removing unavailable article from cart: name={}, quantity removed={}", article.getName(), excess);
				doRemoveFromCart(cart, article, excess);
			}
		}

		if (!missingArticles.isEmpty()) {
			LOGGER.info("Validating order: Missing articles={}, aborting", missingArticles);

			return missingArticles;
		}

		// Update stock
		for (Entry<Article, Long> e : articlesCountInCart.entrySet()) {
			var article = e.getKey();
			var countInCart = e.getValue().intValue();
			int countInStock = article.getStock();
			LOGGER.info("Validating order: Updating stock: name={}, new quantity={}", article.getName(), countInStock - countInCart);
			article.setStock(countInStock - countInCart);
		}

		LOGGER.info("Validating order: All articles are available, proceeding with order creation");

		int amount = articles.stream().mapToInt(Article::getPrice).sum();

		var now = Instant.now();
		var nowAsDate = Date.from(now);
		Instant delivery = now.plus(3, ChronoUnit.DAYS);
		var deliveryAsDate = Date.from(delivery);

		var statusHistory = new StatusHistory();
		statusHistory.setStatus(Status.ORDERED);
		statusHistory.setStatusDate(nowAsDate);
		statusHistory = statusHistoryRepository.save(statusHistory);

		var order = new Order();
		order.setCreatedOn(nowAsDate);
		order.setDeliveryDate(deliveryAsDate);
		order.getArticles().addAll(articles);
		order.setAmount(amount);
		order.setCustomer(customer);
		order.setCurrentStatus(fr.eservices.drive.model.Status.ORDERED);
		order.getHistory().add(statusHistory);
		order = orderRepository.save(order);
		LOGGER.info("Validating order: order.id={}", order.getId());

		var newCart = new Cart();
		newCart.setCreatedOn(nowAsDate);
		newCart.setCustomer(customer);
		newCart = cartRepository.save(newCart);
		LOGGER.info("Validating order: newCart.id={}", newCart.getId());

		customer.setActiveCart(newCart);

		return Collections.emptyList();
	}

	public void addToCart(int customerId, Article article, int quantity) {
		var customer = customerRepository.findOne(customerId);
		var cart = customer.getActiveCart();
		for (var i = 0; i < quantity; i++) {
			cart.getArticles().add(article);
		}
	}

	public void removeFromCart(int customerId, Article article, int quantity) {
		var customer = customerRepository.findOne(customerId);
		var cart = customer.getActiveCart();
		doRemoveFromCart(cart, article, quantity);
	}

	public int countInCart(int customerId, Article article) {
		var customer = customerRepository.findOne(customerId);
		var cart = customer.getActiveCart();
		return (int) cart.getArticles().stream().filter(Predicate.isEqual(article)).count();
	}

	private void doRemoveFromCart(Cart cart, Article article, int quantity) {
		for (var i = 0; i < quantity; i++) {
			Optional<Article> findAny = cart.getArticles().stream().filter(a -> a.getId() == article.getId()).findAny();
			findAny.ifPresent(a -> cart.getArticles().remove(a));
		}
	}

}
