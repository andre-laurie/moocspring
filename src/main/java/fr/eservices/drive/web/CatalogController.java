package fr.eservices.drive.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import fr.eservices.drive.dao.DataException;
import fr.eservices.drive.model.Article;
import fr.eservices.drive.model.Category;
import fr.eservices.drive.repository.ArticleRepository;
import fr.eservices.drive.repository.CategoryRepository;

@Controller
public class CatalogController {

	@Autowired
	private ArticleRepository articleRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@GetMapping(path = { "/index.html", "/catalog/categories.html" })
	public String list(Model model) {

		List<Category> categories = categoryRepository.findAllByOrderByOrderIdxAsc();
		model.addAttribute("categories", categories);
		return "categories";
	}

	@GetMapping(path = "/catalog/category/{categoryId}.html")
	public String categoryContent(Model model,
			@PathVariable int categoryId)
			throws DataException {

		var category = categoryRepository.findOne(categoryId);
		if (category == null) {
			throw new DataException("Category not found: " + categoryId);
		}

		List<Article> articles = articleRepository.findByCategoriesAndStockGreaterThanOrderByNameAsc(category, 0);
		model.addAttribute("articles", articles);
		return "category";
	}

}
