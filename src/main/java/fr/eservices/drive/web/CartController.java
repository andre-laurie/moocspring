package fr.eservices.drive.web;

import java.lang.invoke.MethodHandles;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.eservices.drive.model.Article;
import fr.eservices.drive.repository.ArticleRepository;
import fr.eservices.drive.repository.CustomerRepository;
import fr.eservices.drive.service.LoginService;
import fr.eservices.drive.web.dto.CartEntry;
import fr.eservices.drive.web.dto.CartPojo;
import fr.eservices.drive.web.dto.CartSummaryResponse;
import fr.eservices.drive.web.dto.SimpleResponse;
import fr.eservices.drive.web.dto.SimpleResponse.Status;

@Controller
@RequestMapping("/cart")
public class CartController {

	private static final String WORKING_WITH_TRANSIENT_CART = "Working with transient cart";

	private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	@Autowired
	private ArticleRepository articleRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private LoginService loginService;

	@GetMapping(path = "/preview.html", produces = "text/html")
	public String getCart(Model model,
			@SessionAttribute(required = false) Integer customerId,
			@SessionAttribute(required = false) CartPojo anonymousCart) {

		List<Article> articles;
		if (customerId == null) {
			LOGGER.info(WORKING_WITH_TRANSIENT_CART);
			articles = anonymousCart.getArticles();
		} else {
			var customer = customerRepository.findOne(customerId);
			var activeCart = customer.getActiveCart();
			articles = activeCart.getArticles();
			LOGGER.info("Working with persisted cart {}", activeCart.getId());
		}

		List<Article> distinctArticles = distinctArticles(articles);

		model.addAttribute("articles", distinctArticles);
		return "_cart_header";
	}

	List<Article> distinctArticles(List<Article> articles) {
		Set<Integer> ids = new HashSet<>();
		List<Article> distinctArticles = new ArrayList<>();
		for (Article article : articles) {
			if (!ids.contains(article.getId())) {
				distinctArticles.add(article);
				ids.add(article.getId());
			}
		}
		return distinctArticles;
	}

	@GetMapping(path = "/review.html")
	public String review(Model model,
			@SessionAttribute(required = false) Integer customerId,
			@SessionAttribute(required = false) CartPojo anonymousCart) {

		List<Article> articles;
		if (customerId == null) {
			LOGGER.info(WORKING_WITH_TRANSIENT_CART);
			articles = anonymousCart.getArticles();
		} else {
			var customer = customerRepository.findOne(customerId);
			model.addAttribute("customer", customer);
			var activeCart = customer.getActiveCart();
			articles = activeCart.getArticles();
			LOGGER.info("Working with persisted cart {}", activeCart.getId());
		}

		List<Article> distinclArticles = distinctArticles(articles);

		model.addAttribute("articles", distinclArticles);

		int amount = articles.stream().mapToInt(Article::getPrice).sum();
		model.addAttribute("amount", amount);

		return "review";
	}

	@ResponseBody
	@PostMapping(path = "/add.json", consumes = "application/json", produces = "application/json")
	public SimpleResponse add(@RequestBody CartEntry art,
			@SessionAttribute(required = false) Integer customerId,
			@SessionAttribute(required = false) CartPojo anonymousCart) {

		var res = new SimpleResponse();

		if (art.getQty() <= 0) {
			res.status = Status.ERROR;
			res.message = "Negative qty: " + art.getQty();
			return res;
		}

		var article = articleRepository.findOne(Integer.parseInt(art.getId()));
		if (article == null) {
			res.status = Status.ERROR;
			res.message = "Unexpected article: " + art.getId();
			return res;
		}

		if (customerId == null) {
			LOGGER.info(WORKING_WITH_TRANSIENT_CART);
			for (var i = 0; i < art.getQty(); i++) {
				LOGGER.info("Adding article: qty={} article.id={} to transient cart", art.getQty(), art.getId());
				anonymousCart.getArticles().add(article);
			}
		} else {
			LOGGER.info("Adding article: qty={} article.id={} to persisted cart", art.getQty(), art.getId());
			loginService.addToCart(customerId, article, art.getQty());
		}

		res.status = Status.OK;
		return res;
	}

	@ResponseBody
	@PostMapping(path = "/remove.json", consumes = "application/json", produces = "application/json")
	public SimpleResponse remove(@RequestBody CartEntry art,
			@SessionAttribute(required = false) Integer customerId,
			@SessionAttribute(required = false) CartPojo anonymousCart) {

		var res = new SimpleResponse();

		if (art.getQty() <= 0) {
			res.status = Status.ERROR;
			res.message = "Negative qty: " + art.getQty();
			return res;
		}

		var article = articleRepository.findOne(Integer.parseInt(art.getId()));
		if (article == null) {
			res.status = Status.ERROR;
			res.message = "Unexpected article: " + art.getId();
			return res;
		}

		var entry = new CartEntry();
		entry.setId(art.getId());

		int qty;
		if (customerId == null) {
			LOGGER.info(WORKING_WITH_TRANSIENT_CART);
			for (var i = 0; i < art.getQty(); i++) {
				LOGGER.info("Removing article: qty={} article.id={} to transient cart", art.getQty(), art.getId());
				Optional<Article> findAny = anonymousCart.getArticles().stream().filter(a -> a.getId() == article.getId()).findAny();
				findAny.ifPresent(a -> anonymousCart.getArticles().remove(a));
			}
			qty = (int) anonymousCart.getArticles().stream().filter(a -> a.getId() == article.getId()).count();
		} else {
			LOGGER.info("Removing article: qty={} article.id={} to persisted cart", art.getQty(), art.getId());
			loginService.removeFromCart(customerId, article, art.getQty());
			qty = loginService.countInCart(customerId, article);
		}
		entry.setQty(qty);

		res.entry = entry;
		res.status = Status.OK;
		return res;
	}

	@ResponseBody
	@GetMapping(path = "/summary.json", produces = "application/json")
	public CartSummaryResponse summary(@SessionAttribute(required = false) Integer customerId,
			@SessionAttribute(required = false) CartPojo anonymousCart) {

		var res = new CartSummaryResponse();

		List<Article> articles;
		if (customerId == null) {
			LOGGER.info("Processing summary of transient cart");
			articles = anonymousCart.getArticles();
		} else {

			var customer = customerRepository.findOne(customerId);
			var cart = customer.getActiveCart();
			articles = cart.getArticles();
			LOGGER.info("Processing summary of persisted cart {}", cart.getId());
		}
		if (articles.isEmpty()) {
			res.quantity = "";
			res.amount = "Panier";
		} else {
			res.quantity = String.valueOf(articles.size());
			int amount = articles.stream().mapToInt(Article::getPrice).sum();
			res.amount = NumberFormat.getCurrencyInstance().format(amount / 100f);

			res.entries = articles.stream().collect(Collectors.groupingBy(Article::getId, Collectors.counting()))
					.entrySet()
					.stream()
					.map(e -> {
						var cartEntry = new CartEntry();
						cartEntry.setId(String.valueOf(e.getKey()));
						cartEntry.setQty(e.getValue().intValue());
						return cartEntry;
					}).collect(Collectors.toList());
		}
		return res;
	}

	@GetMapping("/validate.html")
	public ModelAndView validateCart(
			@SessionAttribute Integer customerId,
			RedirectAttributes redirectAttributes) {

		List<Article> missingArticles = loginService.validateOrder(customerId);

		var modelAndView = new ModelAndView();
		if (missingArticles.isEmpty()) {
			LOGGER.info("No missing articles, order validated, proceed with order display");
			modelAndView.setViewName("redirect:/customer/orders.html");
			var confirmation = "Votre commande a bien été validée";
			redirectAttributes.addFlashAttribute("confirmation", confirmation);
		} else {
			LOGGER.info("Missing articles while validating, cart is fixed, back to review");

			var missingArticlesAsString = missingArticles.stream().map(Article::getName).distinct().sorted().collect(Collectors.joining(", "));
			var error = String.format("<b>Avertissement :</b><br>Les articles suivants étaient indisponibles dans la quantité souhaitée. Ils ont été retirés du panier : %s", missingArticlesAsString);
			modelAndView.setViewName("redirect:/cart/review.html");
			redirectAttributes.addFlashAttribute("error", error);
		}

		return modelAndView;
	}

}
