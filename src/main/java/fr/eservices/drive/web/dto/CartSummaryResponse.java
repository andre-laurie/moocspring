package fr.eservices.drive.web.dto;

import java.util.ArrayList;
import java.util.List;

public class CartSummaryResponse {

	public String amount;
	public String quantity;
	public List<CartEntry> entries = new ArrayList<>();

}
