package fr.eservices.drive.web.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.eservices.drive.model.Article;
import fr.eservices.drive.model.Cart;

@SuppressWarnings("serial")
public class CartPojo implements Serializable {

	private Date createdOn;
	private transient List<Article> articles = new ArrayList<>();

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public Cart toCart() {
		var cart = new Cart();
		cart.setCreatedOn(this.getCreatedOn());
		cart.getArticles().addAll(getArticles());
		return cart;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

}
