package fr.eservices.drive.web;

import java.lang.invoke.MethodHandles;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.eservices.drive.model.Article;
import fr.eservices.drive.model.Order;
import fr.eservices.drive.repository.CustomerRepository;
import fr.eservices.drive.repository.OrderRepository;
import fr.eservices.drive.service.LoginService;
import fr.eservices.drive.web.dto.CartPojo;

@Controller
@RequestMapping("/customer")
public class CustomerController {

	private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private LoginService loginService;

	@Autowired
	private OrderRepository orderRepository;

	@GetMapping("/login.html")
	public String login(HttpSession session,
			HttpServletRequest request,
			@SessionAttribute(required = false) String target) {

		if (target == null) {
			String referrer = request.getHeader("referer");
			LOGGER.info("No target set for after successful login, set it to referrer: {}", referrer);
			session.setAttribute("target", referrer);
		}

		return "login";
	}

	@PostMapping("/login.html")
	public ModelAndView processLogin(HttpSession session,
			@RequestParam String email,
			@RequestParam String password,
			@SessionAttribute String target,
			@SessionAttribute CartPojo anonymousCart,
			RedirectAttributes redirectAttributes) {

		var customer = loginService.login(email, password, anonymousCart);
		if (customer == null) {
			var modelAndView = new ModelAndView("login");
			modelAndView.getModelMap().addAttribute("errorMessage", "E-mail ou mot de passe invalide");
			return modelAndView;
		} else {
			// Could be replaced with Spring security
			session.removeAttribute("target");
			session.setAttribute("customerId", customer.getId());
			var welcome = String.format("Bienvenue %s", customer.getLogin());
			redirectAttributes.addFlashAttribute("loginMessageHeader", welcome);
			redirectAttributes.addFlashAttribute("loginMessage", "Les articles du panier ont été regroupés dans le panier actif.");
			return new ModelAndView("redirect:" + target);
		}
	}

	@GetMapping("/logout.html")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:" + "/catalog/categories.html";
	}

	@GetMapping("/orders.html")
	public String orders(Model model,
			@SessionAttribute Integer customerId) {

		var customer = customerRepository.findOne(customerId);
		List<Order> orders = orderRepository.findByCustomerOrderByCreatedOnDesc(customer);
		model.addAttribute("orders", orders);
		return "orders";
	}

	@GetMapping("/order/{orderId}.html")
	public String order(Model model,
			@SessionAttribute Integer customerId,
			@PathVariable int orderId) {

		var order = orderRepository.findOne(orderId);
		model.addAttribute("order", order);

		List<Article> articles = order.getArticles();
		model.addAttribute("articles", articles);

		return "order";
	}

}
