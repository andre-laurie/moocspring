/* global ctx */
/* eslint-env jquery, browser */

$(function () {
	$('.addToCart').click(function () {
		var ref = $(this).parent().data('ref');
		addToCart(ref, true);
	});

	$('.increment').click(function () {
		var ref = $(this).parent().parent().data('ref');
		addToCart(ref, true);
	});

	function addToCart(ref, reload) {
		fetch(ctx + '/cart/add.json', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json;charset=UTF-8' },
			body: JSON.stringify({ id: ref, qty: 1 }),
		})
			.then((response) => {
				if (response.ok) {
					response.json().then((response) => {
						if (response.status === 'OK') {
							if (reload) reloadCart();
							else loadSummary();
						}
					});
				} else console.log('Status error');
			})
			.catch((error) => console.error('error:', error));
	}

	$('.decrement').click(function () {
		var ref = $(this).parent().parent().data('ref');
		removeFromCart(ref, true);
	});

	function removeFromCart(ref, reload) {
		fetch(ctx + '/cart/remove.json', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({ id: ref, qty: 1 }),
		})
			.then((response) => {
				if (response.ok) {
					response.json().then((response) => {
						if (response.status === 'OK') {
							var { id, qty } = response.entry;
							if (reload || qty === 0) reloadCart();
							else {
								loadSummary();
							}
							updateArticleCounter(id, qty);
						}
					});
				} else console.log('Status error');
			})
			.catch((error) => console.error('error:', error));
	}

	function reloadCart() {
		fetch(ctx + '/cart/preview.html')
			.then((response) => {
				if (response.ok) {
					response.text().then((text) => {
						$('#cartInHeader').html(text);
						loadSummary();
						$('.increment-from-cart').off('click');
						$('.increment-from-cart').click(function () {
							var ref = $(this).parent().parent().data('ref');
							addToCart(ref, false);
						});
						$('.decrement-from-cart').off('click');
						$('.decrement-from-cart').click(function () {
							var ref = $(this).parent().parent().data('ref');
							removeFromCart(ref, false);
						});
					});
				} else console.log('Status error');
			})
			.catch((error) => console.error('error:', error));
	}

	function loadSummary() {
		fetch(ctx + '/cart/summary.json')
			.then((response) => {
				if (response.ok) {
					response.json().then(({ amount, quantity, entries }) => {
						console.log('amount: ', amount, 'quantity: ', quantity);

						$('.cart-title').text(amount);
						$('#cart-size').text(quantity);

						entries.forEach(({ id, qty }) => {
							console.log('Article in cart=', id, ' qty=', qty);
							updateArticleCounter(id, qty);
						});
					});
				} else console.log('Status error');
			})
			.catch((error) => console.error('error:', error));
	}

	function updateArticleCounter(id, qty) {
		console.log('updateArticleCounter id=', id, ' qty=', qty);

		if (qty > 0) {
			$(`[data-ref='${id}'] > .btn-group`).removeClass('d-none');
			$(`[data-ref='${id}'] > .addToCart`).addClass('d-none');
		} else {
			$(`[data-ref='${id}'] > .btn-group`).addClass('d-none');
			$(`[data-ref='${id}'] > .addToCart`).removeClass('d-none');
		}
		$(`[data-ref='${id}'] .btn-outline-secondary`).html(qty);
	}

	reloadCart();
});
