<%@include file="_header.jsp" %>
<c:choose>
<c:when test="${empty categories}">
Aucune catégorie
</c:when>
<c:otherwise>
<ul class="categories">
<c:forEach items="${categories}" var="category">
	<li>
		<a href="${ctxPath}/catalog/category/${category.id}.html">
			<img src="${ctxPath}/img/cat0${category.id}.jpg"/><br/>${category.name}
		</a>
	</li>
</c:forEach>
</ul>
</c:otherwise>
</c:choose>
<%@include file="_footer.jsp" %>