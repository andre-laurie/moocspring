<%@include file="_header.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@page import="java.util.List"%>
<div class="row">
	<div class="col-md-12">
		<h3>Votre panier</h3>
	</div>
</div>
<c:choose>
<c:when test="${empty articles}">
Aucun article dans le panier
</c:when>
<c:otherwise>
<c:if test="${! empty error}">
	<div class="row">
		<div class="col-md-12">
			<p class="alert alert-warning" role="alert">${error}</p>
		</div>
	</div>
</c:if>
<div class="d-flex flex-column">
	<c:choose>
		<c:when test="${empty articles }">
		Aucun article dans le panier
		</c:when>
		<c:otherwise>
			<c:forEach items="${articles}" var="article">
			<div class="card d-flex flex-column m-2 p-2">
				<div class="d-flex flex-row mb-2">
					<img class="d-block"
						src="https://static1.chronodrive.com/${article.img}" 
						width="80" height="80"/>
					<div>
						<div class="text-primary"
							style="height: 70px; font-size: 0.9rem;">${article.name}</div>
					</div>
				</div>
				<div class="d-flex justify-content-between align-items-baseline" data-ref="${article.id}">
					<span class="" style="font-size: 1.2rem">
						<strong><fmt:formatNumber type="CURRENCY"
								value="${article.price * .01}" /></strong>
					</span>
					<div class="d-none btn-group align-items-baseline align-items-stretch" role="group">
						<button type="button" class="btn btn-outline-primary decrement">-</button>
						<button type="button" class="btn btn-outline-secondary" disabled></button>
						<button type="button" class="btn btn-outline-primary increment">+</button>
					</div>
					<button class="btn btn-primary addToCart" type="button"><i class="fa fa-shopping-cart"></i></button>
				</div>
			</div>
			</c:forEach>
		</c:otherwise>
	</c:choose>
</div>
<div class="row">
	<div class="col-md-6">
		<span class="h3">Total <span class="cart-title"><fmt:formatNumber type="CURRENCY" value="${amount * .01}" /></span></span>&nbsp;
	</div>
	<div class="col-md-6 text-right">
		<c:choose>
			<c:when test="${empty customer}">
				<a class="btn btn-default active" role="button" href="${ctxPath}/customer/login.html">Se connecter</a>
			</c:when>
			<c:otherwise>
				<a class="btn btn-default active" role="button" href="${ctxPath}/cart/validate.html">Valider</a>
			</c:otherwise>
		</c:choose>
	</div>
</div>
</c:otherwise>
</c:choose>
<%@include file="_footer.jsp" %>