<%@include file="_header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page import="java.util.List"%>
<c:choose>
	<c:when test="${empty articles}">
Aucun article dans la catégorie
</c:when>
	<c:otherwise>


		<div class="row no-gutters">

			<c:forEach items="${articles}" var="article">

				<div class="col-12 col-sm-6 col-md-4 col-lg-3">
					<div class="card d-flex flex-column m-2 p-2">
						<div class="d-flex flex-sm-column mb-2">
							<img class="d-block mx-sm-auto order-sm-2"
								src="https://static1.chronodrive.com/${article.img}" />
							<div class="order-sm-1">
								<div style="height: 70px; font-size: 0.9rem;"><span class="text-primary">${article.name}</span><br>
								<span class="text-secondary">
									<i class="far fa-chart-bar"></i>&nbsp;En stock ${article.stock}</span>
								</div>
							</div>
						</div>
						<div class="d-flex justify-content-between align-items-baseline" data-ref="${article.id}">
							<span class="" style="font-size: 1.2rem">
								<strong><fmt:formatNumber type="CURRENCY"
										value="${article.price * .01}" /></strong>
							</span>
							<div class="d-none btn-group align-items-baseline align-items-stretch" role="group">
								<button type="button" class="btn btn-outline-primary decrement">-</button>
								<button type="button" class="btn btn-outline-secondary" disabled></button>
								<button type="button" class="btn btn-outline-primary increment">+</button>
							</div>
							<button class="btn btn-primary addToCart" type="button"><i class="fa fa-shopping-cart"></i></button>
						</div>
					</div>
				</div>

			</c:forEach>
		</div>
	</c:otherwise>
</c:choose>
<%@include file="_footer.jsp"%>