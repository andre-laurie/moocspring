﻿<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" />

<div class="modal-header">
	<h5 class="modal-title">Aperçu du panier</h5>
	<button type="button" class="close" data-dismiss="modal"
		aria-label="Close">
		<span aria-hidden="true">x</span>
	</button>
</div>
<div class="modal-body d-flex flex-column" style="overflow-y: auto">
	<c:choose>
		<c:when test="${empty articles }">
		Aucun article dans le panier
		</c:when>
		<c:otherwise>
			<c:forEach items="${articles}" var="article">
			<div class="card d-flex flex-column m-2 p-2">
				<div class="d-flex flex-row mb-2">
					<img class="d-block"
						src="https://static1.chronodrive.com/${article.img}" 
						width="80" height="80"/>
					<div>
						<div class="text-primary"
							style="height: 70px; font-size: 0.9rem;">${article.name}</div>
					</div>
				</div>
				<div class="d-flex justify-content-between align-items-baseline" data-ref="${article.id}">
					<span class="" style="font-size: 1.2rem">
						<strong><fmt:formatNumber type="CURRENCY"
								value="${article.price * .01}" /></strong>
					</span>
					<div class="btn-group align-items-baseline align-items-stretch" role="group">
						<button type="button" class="btn btn-outline-primary decrement-from-cart">-</button>
						<button type="button" class="btn btn-outline-secondary" disabled></button>
						<button type="button" class="btn btn-outline-primary increment-from-cart">+</button>
					</div>
				</div>
			</div>
			</c:forEach>
		</c:otherwise>
	</c:choose>
</div>
<c:if test="${! empty articles }">
	<div class="modal-footer">
		<a class="btn btn-primary" role="button"
			href="${ctxPath}/cart/review.html">Voir mon panier</a>
	</div>
</c:if>

