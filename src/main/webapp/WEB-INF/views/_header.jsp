<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" />
<html lang="en">
<% 
java.util.List<String> jsList = new java.util.ArrayList<>();
jsList.add("cart.js");
%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>My Web Drive</title>

      <!-- Bootstrap CSS -->
        <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
            integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
            crossorigin="anonymous"
        />

        <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
        />
        <link href="${ctxPath}/css/main.css" rel="stylesheet"/>
  <script>var ctx="${ctxPath}";</script>
</head>
<body class="bg-light">
  <div class="container">
	<nav class="navbar navbar-expand-md navbar-light bg-white fixed-top shadow-sm">
		<a class="navbar-brand" href="${ctxPath}/catalog/categories.html"><i class="fa fa-store"></i>&nbsp;My Web Drive</a>
	    <ul class="navbar-nav">
	    	<li class="nav-item"><a class="nav-link" href="${ctxPath}/catalog/categories.html">Rayons</a></li>
	    </ul>
		<div class="d-flex order-md-1 ml-auto pr-2 pr-md-0">
			<ul class="navbar-nav">
       			<li class="nav-item">
      				<a class="nav-link py-0" href="#" role="button" data-toggle="modal" data-target="#modal">
      				<span class="d-inline-block text-center" style="line-height:95%;">
      					<i class="fa fa-shopping-cart"></i>&nbsp;<span class="badge badge-pill badge-success" id="cart-size"></span><br>
      					<span class="cart-title">Panier</span>
      				</span>
      				</a>
       			</li>
      		</ul>
		</div>
		
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
         	<span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
	     	<ul class="navbar-nav mr-auto">
	        	<c:if test="${! empty customerId}"><li class="nav-item"><a class="nav-link" href="${ctxPath}/customer/orders.html">Commandes</a></li></c:if>
	        	<li class="nav-item"><a class="nav-link" href="#about">A propos</a></li>
	     	</ul>
      		<ul class="navbar-nav">
      			<li class="nav-item dropdown">
      				<a class="nav-link dropdown-toggle py-0" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
      				<span class="d-inline-block text-center" style="line-height:95%;">
      					<i class="fa fa-user"></i><c:if test="${! empty customerId}">&nbsp;<i class="fa fa-check-circle text-success"></i></c:if><br>
      				Compte
      				</span>
      				</a>
      				<div class="dropdown-menu">
          				<c:choose>
          				<c:when test="${empty customerId}">
              				<a class="dropdown-item" href="${ctxPath}/customer/login.html"><i class="fa fa-sign-in-alt"></i>&nbsp;Connexion</a>
          				</c:when>
          				<c:otherwise>
              				<a class="dropdown-item" href="${ctxPath}/customer/orders.html"><i class="fa fa-list-alt"></i>&nbsp;Commandes</a>
              				<a class="dropdown-item" href="${ctxPath}/customer/logout.html"><i class="fa fa-sign-out-alt"></i>&nbsp;Déconnexion</a>
          				</c:otherwise>
          				</c:choose>
           			</div>
      			</li>
      		</ul>
        </div>
      
	</nav>

	<div class="modal" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog m-0 ml-auto" role="document" style="height: 100%">
			<div class="modal-content bg-light" id="cartInHeader" style="border-radius: 0; height: 100%">
				<!-- This part is replaced with an ajax call -->
			</div>
		</div>
	</div>


	<c:if test="${! empty loginMessage}">
		<div class="alert alert-success" role="alert"><c:if test="${! empty loginMessageHeader}"><strong>${loginMessageHeader}</strong><br></c:if>${loginMessage}</div>
	</c:if>