<%@include file="_header.jsp" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<div class="panel panel-default">
	<div class="panel-heading">D�tail de la commande r�f ${order.id}</div>
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Nom</th>
				<th>Prix</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${articles}" var="article">
			<tr>
				<td>${article.name}</td>
				<td><fmt:formatNumber type="CURRENCY" value="${article.price / 100.}" /></td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
</div>

<%@include file="_footer.jsp" %>