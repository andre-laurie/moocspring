<%@include file="_header.jsp" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<c:choose>
	<c:when test="${empty orders}">
Aucune commande
			</c:when>
	<c:otherwise>
		<c:if test="${! empty confirmation}">
			<div class="alert alert-success" role="alert">${confirmation}</div>
		</c:if>
		<div class="panel panel-default">
	  		<div class="panel-heading">Liste des commandes</div>
	  		<table class="table table-hover">
	  			<thead>
					<tr>
						<th>#</th>
						<th>Date de commande</th>
						<th>Date de livraison</th>
						<th>Montant</th>
						<th>Statut</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${orders}" var="order">
					<tr>
						<td><a href="${ctxPath}/customer/order/${order.id}.html"><span class="glyphicon glyphicon-list-alt"></span> ${order.id}</a></td>
						<td><fmt:formatDate pattern="d/M/YY" value="${order.createdOn}" /></td>
						<td><fmt:formatDate pattern="d/M/YY" value="${order.deliveryDate}" /></td>
						<td><fmt:formatNumber type="CURRENCY" value="${order.amount / 100.}" /></td>
						<td>${order.currentStatus}</td>
					</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</c:otherwise>
</c:choose>
<%@include file="_footer.jsp" %>