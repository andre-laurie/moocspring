<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" />
<html lang="en">
<% 
java.util.List<String> jsList = new java.util.ArrayList<>();
jsList.add("cart.js");
%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>My Web Drive</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/css/ie10-viewport-bug-workaround.css">
  <link href="${ctxPath}/css/signin.css" rel="stylesheet">
  <script>var ctx="${ctxPath}";</script>
</head>

<body>

<div class="container">

	<div class="alert alert-info" role="alert"><strong>Remarque : </strong><br>Il y a 3 comptes prédéfinis dont les e-mail / mot de passe sont : 
		<ul>
		<li>Customer A : a@a / a</li>
		<li>Customer B : b@b / b</li>
		<li>Customer C : c@c / c</li>
		</ul>
	</div>

    <form class="form-signin" action="login.html" method="post">
      <c:if test="${! empty errorMessage}">
       <div class="alert alert-danger" role="alert">${errorMessage}</div>
      </c:if>
      <h2 class="form-signin-heading">Accéder à votre compte</h2>
      <label for="inputEmail" class="sr-only">Email address</label>
      <input id="inputEmail" class="form-control" placeholder="Adresse e-mail" required autofocus type="email" name="email" value="${email}">
      <label for="inputPassword" class="sr-only">Password</label>
      <input id="inputPassword" class="form-control" placeholder="Mot de passe" required type="password" name="password">
      <button class="btn btn-lg btn-primary btn-block" type="submit">Valider</button>
    </form>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="https://maxcdn.bootstrapcdn.com/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>