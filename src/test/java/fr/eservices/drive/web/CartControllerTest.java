package fr.eservices.drive.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.quality.Strictness;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.eservices.drive.model.Article;
import fr.eservices.drive.model.Cart;
import fr.eservices.drive.model.Customer;
import fr.eservices.drive.repository.ArticleRepository;
import fr.eservices.drive.repository.CustomerRepository;
import fr.eservices.drive.service.LoginService;
import fr.eservices.drive.web.dto.CartEntry;
import fr.eservices.drive.web.dto.CartPojo;
import fr.eservices.drive.web.dto.SimpleResponse.Status;

public class CartControllerTest {

	@Rule
	public MockitoRule rule = MockitoJUnit.rule().strictness(Strictness.STRICT_STUBS);

	@Mock
	private ArticleRepository articleRepository;

	@Mock
	private CustomerRepository customerRepository;

	@Mock
	private LoginService loginService;

	@InjectMocks
	private final CartController cartController = new CartController();

	private MockMvc mockMvc;

	@Before
	public void before() {
		mockMvc = standaloneSetup(cartController)
				.setControllerAdvice(new DriveControllerAdvice())
				.build();
	}

	@Test
	public void givenAnonymous_whenGetCart_thenContentsFromSessionCart() throws Exception {
		CartPojo anonymousCart = new CartPojo();
		Article article = new Article();
		anonymousCart.setArticles(List.of(article));

		mockMvc.perform(get("/cart/preview.html")
				.sessionAttr("anonymousCart", anonymousCart))
				.andExpect(status().isOk())
				.andExpect(view().name("_cart_header"))
				.andExpect(model().attribute("articles", List.of(article)));
	}

	@Test
	public void givenAuthenticated_whenGetCart_thenContentsFromDB() throws Exception {
		Cart persistedCart = new Cart();
		Article article = new Article();

		persistedCart.getArticles().add(article);

		Customer customer = new Customer();
		customer.setActiveCart(persistedCart);
		BDDMockito.given(customerRepository.findOne(1)).willReturn(customer);

		mockMvc.perform(get("/cart/preview.html")
				.sessionAttr("customerId", 1))
				.andExpect(status().isOk())
				.andExpect(view().name("_cart_header"))
				.andExpect(model().attribute("articles", List.of(article)));
	}

	@Test
	public void givenDuplicateArticles_whenDistinctArticles_thenDupsRemoved() {
		Article article1 = new Article();
		article1.setId(1);
		Article article2 = new Article();
		article2.setId(2);
		Article article1Dup = new Article();
		article1Dup.setId(1);

		Cart cart = new Cart();
		cart.getArticles().addAll(List.of(article1, article2, article1Dup));

		List<Article> articles = cartController.distinctArticles(cart.getArticles());
		assertThat(articles).containsExactly(article1, article2);
	}

	@Test
	public void givenAnonymous_whenReview_thenArticlesFromSessionCart() throws Exception {
		CartPojo anonymousCart = new CartPojo();
		Article article = new Article();
		article.setPrice(100);

		anonymousCart.setArticles(List.of(article));

		mockMvc.perform(get("/cart/review.html")
				.sessionAttr("anonymousCart", anonymousCart))
				.andExpect(status().isOk())
				.andExpect(view().name("review"))
				.andExpect(model().attribute("articles", List.of(article)))
				.andExpect(model().attribute("amount", 100));
	}

	@Test
	public void givenAuthenticated_whenReview_thenArticlesFromDB() throws Exception {
		Cart persistedCart = new Cart();
		Article article = new Article();
		article.setPrice(100);

		persistedCart.getArticles().add(article);

		Customer customer = new Customer();
		customer.setActiveCart(persistedCart);
		BDDMockito.given(customerRepository.findOne(1)).willReturn(customer);

		mockMvc.perform(get("/cart/review.html")
				.sessionAttr("customerId", 1))
				.andExpect(status().isOk())
				.andExpect(view().name("review"))
				.andExpect(model().attribute("articles", List.of(article)))
				.andExpect(model().attribute("amount", 100));
	}

	@Test
	public void givenInvalidQuantity_whenAdd_thenResponseError() throws Exception {
		CartPojo anonymousCart = new CartPojo();

		CartEntry cartEntry = new CartEntry();
		cartEntry.setId("1");
		cartEntry.setQty(0);

		mockMvc.perform(post("/cart/add.json")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(new ObjectMapper().writeValueAsString(cartEntry))
				.sessionAttr("anonymousCart", anonymousCart))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.status").value(Status.ERROR.name()))
				.andExpect(jsonPath("$.message").value("Negative qty: 0"));
	}

	@Test
	public void givenInvalidArticle_whenAdd_thenResponseError() throws JsonProcessingException, Exception {
		CartPojo anonymousCart = new CartPojo();

		CartEntry cartEntry = new CartEntry();
		cartEntry.setId("1000");
		cartEntry.setQty(1);

		mockMvc.perform(post("/cart/add.json")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(new ObjectMapper().writeValueAsString(cartEntry))
				.sessionAttr("anonymousCart", anonymousCart))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.status").value(Status.ERROR.name()))
				.andExpect(jsonPath("$.message").value("Unexpected article: 1000"));

		assertThat(anonymousCart.getArticles()).isEmpty();
	}

	@Test
	public void givenAnonymousAndValidInput_whenAdd_thenArticleAdded() throws JsonProcessingException, Exception {
		CartPojo anonymousCart = new CartPojo();

		Article article = new Article();
		BDDMockito.given(articleRepository.findOne(1)).willReturn(article);

		CartEntry cartEntry = new CartEntry();
		cartEntry.setId("1");
		cartEntry.setQty(1);

		mockMvc.perform(post("/cart/add.json")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(new ObjectMapper().writeValueAsString(cartEntry))
				.sessionAttr("anonymousCart", anonymousCart))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.status").value(Status.OK.name()));

		assertThat(anonymousCart.getArticles()).containsExactly(article);
	}

	@Test
	public void givenAuthenticatedAndValidInput_whenAdd_thenArticleAdded() throws JsonProcessingException, Exception {
		Article article = new Article();
		BDDMockito.given(articleRepository.findOne(1)).willReturn(article);

		CartEntry cartEntry = new CartEntry();
		cartEntry.setId("1");
		cartEntry.setQty(1);

		mockMvc.perform(post("/cart/add.json")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(new ObjectMapper().writeValueAsString(cartEntry))
				.sessionAttr("customerId", 1))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.status").value(Status.OK.name()));

		BDDMockito.then(loginService).should().addToCart(1, article, 1);
	}

	@Test
	public void givenInvalidQuantity_whenRemove_thenResponseError() throws JsonProcessingException, Exception {
		CartPojo anonymousCart = new CartPojo();

		CartEntry cartEntry = new CartEntry();
		cartEntry.setId("1");
		cartEntry.setQty(0);

		mockMvc.perform(post("/cart/remove.json")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(new ObjectMapper().writeValueAsString(cartEntry))
				.sessionAttr("anonymousCart", anonymousCart))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.status").value(Status.ERROR.name()))
				.andExpect(jsonPath("$.message").value("Negative qty: 0"));

		assertThat(anonymousCart.getArticles()).isEmpty();
	}

	@Test
	public void giventInvalidArticle_whenRemove_thenResponseError() throws JsonProcessingException, Exception {
		var anonymousCart = new CartPojo();

		CartEntry cartEntry = new CartEntry();
		cartEntry.setId("1000");
		cartEntry.setQty(1);

		mockMvc.perform(post("/cart/remove.json")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(new ObjectMapper().writeValueAsString(cartEntry))
				.sessionAttr("anonymousCart", anonymousCart))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.status").value(Status.ERROR.name()))
				.andExpect(jsonPath("$.message").value("Unexpected article: 1000"));

		assertThat(anonymousCart.getArticles()).isEmpty();
	}

	@Test
	public void givenAnonymousAndValidInput_whenRemove_thenArticleRemoved() throws JsonProcessingException, Exception {
		Article article = new Article();
		article.setId(1);
		List<Article> articles = new ArrayList<>();
		var anonymousCart = new CartPojo();
		articles.add(article);
		articles.add(article);
		anonymousCart.setArticles(articles);

		BDDMockito.given(articleRepository.findOne(1)).willReturn(article);

		CartEntry cartEntry = new CartEntry();
		cartEntry.setId("1");
		cartEntry.setQty(1);

		mockMvc.perform(post("/cart/remove.json")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(new ObjectMapper().writeValueAsString(cartEntry))
				.sessionAttr("anonymousCart", anonymousCart))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.status").value(Status.OK.name()))
				.andExpect(jsonPath("$.entry.id").value("1"))
				.andExpect(jsonPath("$.entry.qty").value("1"));

		assertThat(anonymousCart.getArticles()).containsExactly(article);
	}

	@Test
	public void givenAnonymousAndValidInputAndArticleNotInCart_whenRemove_thenDoNothing() throws JsonProcessingException, Exception {
		Article article = new Article();
		article.setId(2);
		List<Article> articles = new ArrayList<>();
		articles.add(article);
		var anonymousCart = new CartPojo();
		anonymousCart.setArticles(articles);

		Article articleNotInCart = new Article();
		articleNotInCart.setId(1);
		BDDMockito.given(articleRepository.findOne(1)).willReturn(articleNotInCart);

		CartEntry cartEntry = new CartEntry();
		cartEntry.setId("1");
		cartEntry.setQty(1);

		mockMvc.perform(post("/cart/remove.json")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(new ObjectMapper().writeValueAsString(cartEntry))
				.sessionAttr("anonymousCart", anonymousCart))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.status").value(Status.OK.name()))
				.andExpect(jsonPath("$.entry.id").value("1"))
				.andExpect(jsonPath("$.entry.qty").value("0"));

		assertThat(anonymousCart.getArticles()).containsExactly(article);
	}

	@Test
	public void givenAuthenticatedAndValidInput_whenRemove_thenArticleRemoved() throws JsonProcessingException, Exception {
		Article article = new Article();
		BDDMockito.given(articleRepository.findOne(1)).willReturn(article);

		CartEntry cartEntry = new CartEntry();
		cartEntry.setId("1");
		cartEntry.setQty(1);

		mockMvc.perform(post("/cart/remove.json")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(new ObjectMapper().writeValueAsString(cartEntry))
				.sessionAttr("customerId", 1))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.status").value(Status.OK.name()))
				.andExpect(jsonPath("$.entry.id").value("1"))
				.andExpect(jsonPath("$.entry.qty").value("0"));

		BDDMockito.then(loginService).should().removeFromCart(1, article, 1);
	}

	@Test
	public void givenAnonymousAndEmptyCart_whenSummary_thenQuantityAndAmountDefaultFromSessionCart() throws Exception {
		var anonymousCart = new CartPojo();

		mockMvc.perform(get("/cart/summary.json")
				.sessionAttr("anonymousCart", anonymousCart))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.quantity").value(""))
				.andExpect(jsonPath("$.amount").value("Panier"));
	}

	@Test
	public void givenAuthenticatedAndEmptyCart_whenSummary_thenQuantityAndAmountDefaultFromSessionCart() throws Exception {
		Cart persistedCart = new Cart();
		Customer customer = new Customer();
		customer.setActiveCart(persistedCart);

		BDDMockito.given(customerRepository.findOne(1)).willReturn(customer);

		mockMvc.perform(get("/cart/summary.json")
				.sessionAttr("customerId", 1))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.quantity").value(""))
				.andExpect(jsonPath("$.amount").value("Panier"));
	}

	@Test
	public void givenAnonymousAndNotEmptyCart_whenSummary_thenQuantityAndAmountDefaultFromSessionCart() throws Exception {
		var anonymousCart = new CartPojo();
		Article article = new Article();
		article.setId(1);
		article.setPrice(100);
		anonymousCart.setArticles(List.of(article));

		mockMvc.perform(get("/cart/summary.json")
				.sessionAttr("anonymousCart", anonymousCart))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.quantity").value("1"))
				.andExpect(jsonPath("$.amount").value(NumberFormat.getCurrencyInstance().format(1f)))
				.andExpect(jsonPath("$.entries.length()").value("1"))
				.andExpect(jsonPath("$.entries[0].id").value("1"))
				.andExpect(jsonPath("$.entries[0].qty").value("1"));
	}

	@Test
	public void givenAuthenticatedAndNotEmptyCart_whenSummary_thenQuantityAndAmountDefaultFromSessionCart() throws Exception {
		Cart persistedCart = new Cart();
		Customer customer = new Customer();
		customer.setActiveCart(persistedCart);

		BDDMockito.given(customerRepository.findOne(1)).willReturn(customer);
		Article article = new Article();
		article.setId(1);
		article.setPrice(100);
		persistedCart.getArticles().add(article);

		mockMvc.perform(get("/cart/summary.json")
				.sessionAttr("customerId", 1))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.quantity").value("1"))
				.andExpect(jsonPath("$.amount").value(NumberFormat.getCurrencyInstance().format(1f)))
				.andExpect(jsonPath("$.entries.length()").value("1"))
				.andExpect(jsonPath("$.entries[0].id").value("1"))
				.andExpect(jsonPath("$.entries[0].qty").value("1"));
	}

	@Test
	public void givenNoMissingArticles_whenValidate_thenOrder() throws Exception {
		BDDMockito.given(loginService.validateOrder(1)).willReturn(List.of());

		mockMvc.perform(get("/cart/validate.html")
				.sessionAttr("customerId", 1))
				.andExpect(status().is3xxRedirection())
				.andExpect(redirectedUrl("/customer/orders.html"))
				.andExpect(flash().attribute("confirmation", "Votre commande a bien été validée"));
	}

	@Test
	public void givenMissingArticles_whenValidate_thenReviewFixedCart() throws Exception {
		Article article = new Article();
		article.setName("Article1");
		BDDMockito.given(loginService.validateOrder(1)).willReturn(List.of(article));

		mockMvc.perform(get("/cart/validate.html")
				.sessionAttr("customerId", 1))
				.andExpect(status().is3xxRedirection())
				.andExpect(redirectedUrl("/cart/review.html"))
				.andExpect(flash().attribute("error", "<b>Avertissement :</b><br>Les articles suivants étaient indisponibles dans la quantité souhaitée. Ils ont été retirés du panier : Article1"));
	}
}
