package fr.eservices.drive.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.quality.Strictness;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.web.servlet.MockMvc;

import fr.eservices.drive.model.Article;
import fr.eservices.drive.model.Customer;
import fr.eservices.drive.model.Order;
import fr.eservices.drive.repository.CustomerRepository;
import fr.eservices.drive.repository.OrderRepository;
import fr.eservices.drive.service.LoginService;
import fr.eservices.drive.web.dto.CartPojo;

public class CustomerControllerTest {

	@Rule
	public MockitoRule rule = MockitoJUnit.rule().strictness(Strictness.STRICT_STUBS);

	@Mock
	private CustomerRepository customerRepository;

	@Mock
	private LoginService loginService;

	@Mock
	private OrderRepository orderRepository;

	private MockMvc mockMvc;

	@InjectMocks
	private final CustomerController customerController = new CustomerController();

	@Before
	public void before() {
		mockMvc = standaloneSetup(customerController)
				.setControllerAdvice(new DriveControllerAdvice())
				.build();
	}

	@Test
	public void givenTargetAlreadySet_whenLogin_thenTargetNotChanged() throws Exception {
		HttpSession session = mockMvc.perform(get("/customer/login.html")
				.sessionAttr("target", "original")
				.header("referer", "new"))
				.andExpect(status().isOk())
				.andExpect(view().name("login"))
				.andReturn()
				.getRequest()
				.getSession();

		assertThat(session.getAttribute("target")).isEqualTo("original");
	}

	@Test
	public void givenTargetNotSet_whenLogin_thenTargetSetFromReferer() throws Exception {
		HttpSession session = mockMvc.perform(get("/customer/login.html")
				.header("referer", "from"))
				.andExpect(status().isOk())
				.andExpect(view().name("login"))
				.andReturn()
				.getRequest()
				.getSession();

		assertThat(session.getAttribute("target")).isEqualTo("from");
	}

	@Test
	public void givenReferer_whenLogin_thenTargetSet() throws Exception {
		HttpSession session = mockMvc.perform(get("/customer/login.html").header("referer", "from"))
				.andExpect(status().isOk())
				.andExpect(view().name("login"))
				.andReturn()
				.getRequest()
				.getSession();

		assertThat(session.getAttribute("target")).isEqualTo("from");
	}

	@Test
	public void givenNoEmail_whenProcessLogin_thenException() throws Exception {
		mockMvc.perform(post("/customer/login.html")
				.param("password", "password")
				.sessionAttr("target", "target"))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void givenNoPassword_whenProcessLogin_thenException() throws Exception {
		mockMvc.perform(post("/customer/login.html")
				.param("email", "email")
				.sessionAttr("target", "target"))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void givenUnknownCredentials_whenProcessLogin_thenLoginPageWithErrorMessage() throws Exception {
		BDDMockito.given(loginService.login(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.any(CartPojo.class))).willReturn(null);

		mockMvc.perform(post("/customer/login.html")
				.param("email", "email")
				.param("password", "password")
				.sessionAttr("target", "target"))
				.andExpect(status().isOk())
				.andExpect(view().name("login"))
				.andExpect(model().attribute("errorMessage", "E-mail ou mot de passe invalide"));
	}

	@Test
	public void givenValidCredentials_whenProcessLogin_thenRedirectToReferingPage() throws Exception {
		Customer customer = new Customer();
		customer.setId(1);
		customer.setLogin("login");
		BDDMockito.given(loginService.login(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.any(CartPojo.class))).willReturn(customer);

		mockMvc.perform(post("/customer/login.html")
				.param("email", "email")
				.param("password", "password")
				.sessionAttr("target", "target"))
				.andExpect(status().is3xxRedirection())
				.andExpect(redirectedUrl("target"))
				.andExpect(flash().attribute("loginMessageHeader", "Bienvenue login"))
				.andExpect(flash().attribute("loginMessage", "Les articles du panier ont été regroupés dans le panier actif."));
	}

	@Test
	public void whenLogout_thenSessionInvalidated() throws Exception {
		MockHttpSession session = new MockHttpSession();

		mockMvc.perform(get("/customer/logout.html")
				.session(session))
				.andExpect(status().is3xxRedirection())
				.andExpect(redirectedUrl("/catalog/categories.html"));

		assertTrue(session.isInvalid());
	}

	@Test
	public void whenOrders_thenOrderList() throws Exception {
		List<Order> orders = List.of(new Order());
		BDDMockito.given(orderRepository.findByCustomerOrderByCreatedOnDesc(ArgumentMatchers.any())).willReturn(orders);

		mockMvc.perform(get("/customer/orders.html")
				.sessionAttr("customerId", 1))
				.andExpect(status().isOk())
				.andExpect(view().name("orders"))
				.andExpect(model().attribute("orders", orders));
	}

	@Test
	public void whenOrder_thenOrderContents() throws Exception {
		Order order = new Order();
		order.getArticles().add(new Article());

		BDDMockito.given(orderRepository.findOne(2)).willReturn(order);

		mockMvc.perform(get("/customer/order/{orderId}.html", 2)
				.sessionAttr("customerId", 1))
				.andExpect(status().isOk())
				.andExpect(view().name("order"))
				.andExpect(model().attribute("order", order));
	}

}
