package fr.eservices.drive.web;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.quality.Strictness;
import org.springframework.test.web.servlet.MockMvc;

import fr.eservices.drive.dao.DataException;
import fr.eservices.drive.model.Article;
import fr.eservices.drive.model.Category;
import fr.eservices.drive.repository.ArticleRepository;
import fr.eservices.drive.repository.CategoryRepository;

public class CatalogControllerTest {

	@Rule
	public MockitoRule rule = MockitoJUnit.rule().strictness(Strictness.STRICT_STUBS);

	@Mock
	private ArticleRepository articleRepository;

	@Mock
	private CategoryRepository categoryRepository;

	@InjectMocks
	private final CatalogController catalogController = new CatalogController();

	private MockMvc mockMvc;

	@Before
	public void before() {
		mockMvc = standaloneSetup(catalogController)
				.setControllerAdvice(new DriveControllerAdvice())
				.build();
	}

	@Test
	public void whenList_thenCategories() throws Exception {
		Category c = new Category();
		List<Category> categories = List.of(c);

		BDDMockito.given(categoryRepository.findAllByOrderByOrderIdxAsc()).willReturn(categories);

		mockMvc.perform(get("/catalog/categories.html"))
				.andExpect(status().isOk())
				.andExpect(model().attribute("categories", categories))
				.andExpect(view().name("categories"));
	}

	@Test
	public void whenIndex_thenMapped() throws Exception {
		Category c = new Category();
		List<Category> categories = List.of(c);

		BDDMockito.given(categoryRepository.findAllByOrderByOrderIdxAsc()).willReturn(categories);

		mockMvc.perform(get("/index.html"))
				.andExpect(status().isOk())
				.andExpect(model().attribute("categories", categories))
				.andExpect(view().name("categories"));
	}

	@Test
	public void givenUnknownCategory_whenCategoryContent_thenException() throws Exception {
		BDDMockito.given(categoryRepository.findOne(ArgumentMatchers.anyInt())).willReturn(null);

		mockMvc.perform(get("/catalog/category/{categoryId}.html", 0))
				.andExpect(status().isBadRequest())
				.andExpect(result -> assertTrue(result.getResolvedException() instanceof DataException));
	}

	@Test
	public void givenValidCategory_whenCategoryContent_thenArticles() throws Exception {
		Category category = new Category();
		BDDMockito.given(categoryRepository.findOne(ArgumentMatchers.anyInt())).willReturn(category);

		Article article = new Article();
		List<Article> articles = List.of(article);
		BDDMockito.given(articleRepository.findByCategoriesAndStockGreaterThanOrderByNameAsc(category, 0)).willReturn(articles);

		mockMvc.perform(get("/catalog/category/{categoryId}.html", 0))
				.andExpect(status().isOk())
				.andExpect(model().attribute("articles", articles))
				.andExpect(view().name("category"));
	}

}
