package fr.eservices.drive.web;

import static org.assertj.core.api.Assertions.assertThat;

import javax.sql.DataSource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.eservices.drive.config.DataConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DataConfig.class)
@TestPropertySource(properties = { "DATABASE_URL=postgres://username:password@localhost:5432/drive" })
public class DataConfigTest {

	@Autowired
	DataSource dataSource;

	@Test
	public void givenDatabaseUrlAsEnvVar_whenInit_thenDataSourceOk() {
		DriverManagerDataSource driverManagerDataSource = (DriverManagerDataSource) dataSource;
		assertThat(driverManagerDataSource.getUsername()).isEqualTo("username");
		assertThat(driverManagerDataSource.getPassword()).isEqualTo("password");
		assertThat(driverManagerDataSource.getUrl()).isEqualTo("jdbc:postgresql://localhost:5432/drive");
	}

}
