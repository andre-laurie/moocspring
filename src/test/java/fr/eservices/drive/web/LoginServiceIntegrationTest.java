package fr.eservices.drive.web;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.eservices.drive.dao.DataException;
import fr.eservices.drive.model.Article;
import fr.eservices.drive.model.Cart;
import fr.eservices.drive.model.Customer;
import fr.eservices.drive.model.Order;
import fr.eservices.drive.model.Status;
import fr.eservices.drive.model.StatusHistory;
import fr.eservices.drive.repository.ArticleRepository;
import fr.eservices.drive.repository.CartRepository;
import fr.eservices.drive.repository.CustomerRepository;
import fr.eservices.drive.repository.OrderRepository;
import fr.eservices.drive.service.LoginService;
import fr.eservices.drive.web.dto.CartPojo;

@RunWith(SpringJUnit4ClassRunner.class)
@IntegrationTestConfiguration
@Transactional
public class LoginServiceIntegrationTest {

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private ArticleRepository articleRepository;

	@Autowired
	private LoginService loginService;

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private CartRepository cartRepository;

	private Customer cust;

	private Article article;

	@Before
	public void before() {
		cust = new Customer();
		cust.setEmail("a@a");
		cust.setPassword("BGfeiTOXva/mSnoluSgIYA==");
		cust = customerRepository.save(cust);

		article = new Article();
		article = articleRepository.save(article);

		entityManager.flush();
	}

	@Test
	public void givenInvalidCredentials_whenLogin_thenReturnNull() {
		CartPojo anonymousCart = new CartPojo();

		Customer customer = loginService.login("invalid", "a", anonymousCart);
		assertThat(customer).isNull();
	}

	@Test
	public void givenNoActiveCart_whenLogin_thenNewCartCreated() {
		CartPojo anonymousCart = new CartPojo();
		anonymousCart.getArticles().add(article);

		Customer customer = loginService.login("a@a", "a", anonymousCart);
		entityManager.flush();

		assertThat(customer).isEqualTo(cust);
		Cart activeCart = customer.getActiveCart();
		assertThat(activeCart.getId()).isPositive();
		assertThat(activeCart.getCustomer()).isNotNull();
		assertThat(activeCart.getArticles()).contains(article);
	}

	@Test
	public void givenActiveCart_whenLogin_thenCartMerged() {
		Cart cart = new Cart();
		cart = cartRepository.save(cart);
		cust.setActiveCart(cart);
		cart.setCustomer(cust);
		entityManager.flush();

		CartPojo anonymousCart = new CartPojo();

		anonymousCart.getArticles().add(article);
		Customer customer = loginService.login("a@a", "a", anonymousCart);
		entityManager.flush();

		Cart activeCart = customer.getActiveCart();
		assertThat(activeCart.getArticles()).contains(article);
	}

	@Test
	public void givenCustomerWithAvailableArticle_whenValidateOrder_thenOrderCreatedAndStockUpdated() throws DataException {
		Cart cart = new Cart();
		cart = cartRepository.save(cart);
		cust.setActiveCart(cart);
		cart.setCustomer(cust);
		cart.getArticles().add(article);
		article.setStock(1);
		entityManager.flush();

		List<Article> articles = loginService.validateOrder(cust.getId());
		entityManager.flush();

		assertThat(articles).isEmpty();
		assertThat(article.getStock()).isZero();

		Cart activeCart = cust.getActiveCart();
		assertThat(activeCart).isNotEqualTo(cart);
		List<Order> orders = orderRepository.findByCustomerOrderByCreatedOnDesc(cust);
		assertThat(orders).hasSize(1);
		Order order = orders.get(0);
		List<Article> orderArticles = order.getArticles();
		assertThat(orderArticles).hasSize(1);
		assertThat(orderArticles.get(0)).isEqualTo(article);

		assertThat(order.getCurrentStatus()).isEqualTo(Status.ORDERED);
		List<StatusHistory> history = order.getHistory();
		assertThat(history).hasSize(1);
		StatusHistory statusHistory = history.get(0);
		assertThat(statusHistory.getStatus()).isEqualTo(Status.ORDERED);

		Order orderWithArticles = orderRepository.findOne(order.getId());
		assertThat(orderWithArticles).isEqualTo(order);
	}

}
