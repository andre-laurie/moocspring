package fr.eservices.drive.web;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;

import org.junit.Test;
import org.springframework.mock.web.MockHttpSession;

import fr.eservices.drive.model.Cart;
import fr.eservices.drive.web.dto.CartPojo;

public class DriveControllerAdviceTest {

	private final DriveControllerAdvice driveControllerAdvice = new DriveControllerAdvice();

	@Test
	public void whenAuthenticated_thenDoNothing() {
		MockHttpSession session = new MockHttpSession();

		driveControllerAdvice.addTransientCartIntoSession(session, 1, null);

		assertThat(session.getAttribute("anonymousCart")).isNull();
	}

	@Test
	public void whenAnonymousWithoutCart_thenCreateCart() {
		MockHttpSession session = new MockHttpSession();

		driveControllerAdvice.addTransientCartIntoSession(session, null, null);

		CartPojo anonymousCart = (CartPojo) session.getAttribute("anonymousCart");
		assertThat(anonymousCart.getCreatedOn()).isCloseTo(Instant.now(), 1000L);
	}

	@Test
	public void whenAnonymousWithCart_thenDoNothing() {
		MockHttpSession session = new MockHttpSession();

		driveControllerAdvice.addTransientCartIntoSession(session, null, new Cart());

		assertThat(session.getAttribute("anonymousCart")).isNull();
	}
}
