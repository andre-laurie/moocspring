package fr.eservices.drive.web;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.quality.Strictness;

import fr.eservices.drive.model.Article;
import fr.eservices.drive.model.Cart;
import fr.eservices.drive.model.Customer;
import fr.eservices.drive.model.Order;
import fr.eservices.drive.model.Status;
import fr.eservices.drive.model.StatusHistory;
import fr.eservices.drive.repository.CartRepository;
import fr.eservices.drive.repository.CustomerRepository;
import fr.eservices.drive.repository.OrderRepository;
import fr.eservices.drive.repository.StatusHistoryRepository;
import fr.eservices.drive.service.LoginService;
import fr.eservices.drive.service.PasswordChecker;
import fr.eservices.drive.web.dto.CartPojo;

public class LoginServiceTest {

	@Rule
	public MockitoRule rule = MockitoJUnit.rule().strictness(Strictness.STRICT_STUBS);

	@Mock
	private CustomerRepository customerRepository;

	@Mock
	private CartRepository cartRepository;

	@Mock
	private OrderRepository orderRepository;

	@Mock
	private StatusHistoryRepository statusHistoryRepository;

	@Mock
	private PasswordChecker passwordChecker;

	@InjectMocks
	private final LoginService loginService = new LoginService();

	@Test
	public void givenInvalidCredentials_whenLogin_thenReturnNull() {
		BDDMockito.given(passwordChecker.encode("email", "password")).willReturn("encoded");
		BDDMockito.given(customerRepository.findCustomerByEmailAndPassword("email", "encoded")).willReturn(null);

		assertThat(loginService.login("email", "password", new CartPojo())).isNull();
	}

	@Test
	public void givenValidCredentialsAndNoActiveCart_whenLogin_thenAnonymousCartIsPersisted() {
		BDDMockito.given(passwordChecker.encode("email", "password")).willReturn("encoded");
		Customer customer = new Customer();
		BDDMockito.given(customerRepository.findCustomerByEmailAndPassword("email", "encoded")).willReturn(customer);

		Cart cart = new Cart();
		BDDMockito.given(cartRepository.save(ArgumentMatchers.any(Cart.class))).willReturn(cart);

		assertThat(loginService.login("email", "password", new CartPojo())).isEqualTo(customer);

		assertThat(customer.getActiveCart()).isEqualTo(cart);

		ArgumentCaptor<Cart> cartCaptor = ArgumentCaptor.forClass(Cart.class);
		BDDMockito.then(cartRepository).should().save(cartCaptor.capture());
		Cart persistedCart = cartCaptor.getValue();
		assertThat(persistedCart.getCustomer()).isEqualTo(customer);
	}

	@Test
	public void givenValidCredentialsAndActiveCart_whenLogin_thenMerge() {
		BDDMockito.given(passwordChecker.encode("email", "password")).willReturn("encoded");
		Customer customer = new Customer();
		Cart activeCart = new Cart();
		Article previousArticle = new Article();
		activeCart.getArticles().add(previousArticle);
		customer.setActiveCart(activeCart);
		BDDMockito.given(customerRepository.findCustomerByEmailAndPassword("email", "encoded")).willReturn(customer);

		CartPojo cartPojo = new CartPojo();
		Article newArticle = new Article();
		cartPojo.getArticles().add(newArticle);

		assertThat(loginService.login("email", "password", cartPojo)).isEqualTo(customer);
		assertThat(activeCart.getArticles()).containsExactlyInAnyOrder(previousArticle, newArticle);
	}

	@Test
	public void givenMissingArticle_whenValidateOrder_thenReturnMissingArticle() {
		Customer customer = new Customer();
		Cart activeCart = new Cart();
		customer.setActiveCart(activeCart);
		Article article = new Article();
		article.setStock(1);
		activeCart.getArticles().add(article);
		activeCart.getArticles().add(article);
		BDDMockito.given(customerRepository.findOne(ArgumentMatchers.anyInt())).willReturn(customer);

		assertThat(loginService.validateOrder(1)).containsExactly(article);

		assertThat(activeCart.getArticles()).containsExactly(article);
	}

	@Test
	public void givenNoMissingArticle_whenValidateOrder_thenEmptyListAndStockUpdatedAndOrderCreated() {
		Customer customer = new Customer();
		Cart activeCart = new Cart();
		customer.setActiveCart(activeCart);
		Article article = new Article();
		article.setPrice(100);
		article.setStock(1);
		activeCart.getArticles().add(article);
		BDDMockito.given(customerRepository.findOne(ArgumentMatchers.anyInt())).willReturn(customer);

		BDDMockito.given(statusHistoryRepository.save(ArgumentMatchers.any(StatusHistory.class))).willAnswer(invocation -> invocation.getArgument(0));
		BDDMockito.given(orderRepository.save(ArgumentMatchers.any(Order.class))).willAnswer(invocation -> invocation.getArgument(0));
		BDDMockito.given(cartRepository.save(ArgumentMatchers.any(Cart.class))).willAnswer(invocation -> invocation.getArgument(0));

		List<Article> missingArticles = loginService.validateOrder(1);

		// No missing articles
		assertThat(missingArticles).isEmpty();

		// Stock updated
		assertThat(article.getStock()).isZero();

		// StatusHistory persisted
		ArgumentCaptor<StatusHistory> statusHistoryCaptor = ArgumentCaptor.forClass(StatusHistory.class);
		BDDMockito.then(statusHistoryRepository).should().save(statusHistoryCaptor.capture());
		StatusHistory statusHistory = statusHistoryCaptor.getValue();
		assertThat(statusHistory.getStatus()).isEqualTo(Status.ORDERED);
		assertThat(statusHistory.getStatusDate()).isCloseTo(Instant.now(), 1000L);

		// Order persisted
		ArgumentCaptor<Order> orderCaptor = ArgumentCaptor.forClass(Order.class);
		BDDMockito.then(orderRepository).should().save(orderCaptor.capture());
		Order order = orderCaptor.getValue();
		assertThat(order.getArticles()).containsExactly(article);
		assertThat(order.getAmount()).isEqualTo(100);
		assertThat(order.getCreatedOn()).isCloseTo(Instant.now(), 1000L);
		assertThat(order.getDeliveryDate()).isAfter(Instant.now());
		assertThat(order.getCustomer()).isEqualTo(customer);
		assertThat(order.getCurrentStatus()).isEqualTo(Status.ORDERED);
		assertThat(order.getHistory()).containsExactly(statusHistory);

		// New cart assigned and set active
		ArgumentCaptor<Cart> cartCaptor = ArgumentCaptor.forClass(Cart.class);
		BDDMockito.then(cartRepository).should().save(cartCaptor.capture());
		Cart cart = cartCaptor.getValue();
		assertThat(cart.getCreatedOn()).isCloseTo(Instant.now(), 1000L);
		assertThat(cart.getCustomer()).isEqualTo(customer);
		assertThat(customer.getActiveCart()).isEqualTo(cart);
	}

	@Test
	public void givenCustomerAndArticle_whenAddToCart_thenArticleAdded() {
		Customer customer = new Customer();
		Cart activeCart = new Cart();
		customer.setActiveCart(activeCart);
		BDDMockito.given(customerRepository.findOne(ArgumentMatchers.anyInt())).willReturn(customer);

		Article article = new Article();

		loginService.addToCart(0, article, 1);

		assertThat(activeCart.getArticles()).containsExactly(article);
	}

	@Test
	public void givenCustomerAndArticle_whenRemoveFromCart_thenArticleRemoved() {
		Customer customer = new Customer();
		Cart activeCart = new Cart();
		customer.setActiveCart(activeCart);
		Article article = new Article();
		activeCart.getArticles().add(article);
		BDDMockito.given(customerRepository.findOne(ArgumentMatchers.anyInt())).willReturn(customer);

		loginService.removeFromCart(0, article, 1);

		assertThat(activeCart.getArticles()).isEmpty();
	}

	@Test
	public void givenCustomerAndArticleNotInCart_whenRemoveFromCart_thenDoNothing() {
		Customer customer = new Customer();
		Cart activeCart = new Cart();
		customer.setActiveCart(activeCart);
		Article articleInCart = new Article();
		articleInCart.setId(1);
		activeCart.getArticles().add(articleInCart);
		BDDMockito.given(customerRepository.findOne(ArgumentMatchers.anyInt())).willReturn(customer);

		Article article = new Article();
		article.setId(2);
		loginService.removeFromCart(0, article, 1);

		assertThat(activeCart.getArticles()).containsExactly(articleInCart);
	}

	@Test
	public void giverCustomerAndArticle_whenCountInCart_thenReturnCount() {
		Customer customer = new Customer();
		Cart activeCart = new Cart();
		customer.setActiveCart(activeCart);
		Article article = new Article();
		activeCart.getArticles().add(article);
		BDDMockito.given(customerRepository.findOne(ArgumentMatchers.anyInt())).willReturn(customer);

		int count = loginService.countInCart(0, article);

		assertThat(count).isOne();
	}

}
