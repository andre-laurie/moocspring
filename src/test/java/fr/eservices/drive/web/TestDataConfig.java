package fr.eservices.drive.web;

import java.lang.invoke.MethodHandles;
import java.net.URISyntaxException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
public class TestDataConfig {

	private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	@Bean
	public DataSource dataSource() throws URISyntaxException {

		String dbUrl = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
		LOGGER.info("dbUrl: {}", dbUrl);

		var dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("org.h2.Driver");
		dataSource.setUrl(dbUrl);
		return dataSource;
	}

}
