package fr.eservices.drive.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.text.NumberFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Commit;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.eservices.drive.model.Article;
import fr.eservices.drive.model.Cart;
import fr.eservices.drive.model.Category;
import fr.eservices.drive.model.Customer;
import fr.eservices.drive.model.Order;
import fr.eservices.drive.model.Perishable;
import fr.eservices.drive.model.Product;
import fr.eservices.drive.model.Status;
import fr.eservices.drive.model.StatusHistory;
import fr.eservices.drive.repository.ArticleRepository;
import fr.eservices.drive.repository.CategoryRepository;
import fr.eservices.drive.repository.CustomerRepository;
import fr.eservices.drive.repository.OrderRepository;
import fr.eservices.drive.repository.StatusHistoryRepository;
import fr.eservices.drive.web.dto.CartEntry;
import fr.eservices.drive.web.dto.CartPojo;

@RunWith(SpringJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@IntegrationTestConfiguration
@DirtiesContext
public class UserAcceptanceTest {

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private StatusHistoryRepository statusHistoryRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private ArticleRepository articleRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	@Before
	public void before() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	@Test
	@Transactional
	@Commit
	public void test1_givenEmptyDB_whenInit_thenNoCrash() {
		Category category = new Category();
		category.setName("category");
		category.setOrderIdx(1);
		category = categoryRepository.save(category);
		assertThat(category.getId()).isPositive();

		Article article = new Article();
		article.setEan("ean");
		article.setImg("img");
		article.setName("article");
		article.setPrice(100);
		article.setStock(1);
		article.setVat(0.2);
		article = articleRepository.save(article);
		article.getCategories().add(category);
		assertThat(article.getId()).isPositive();

		Perishable perishable = new Perishable();
		perishable.setBestBefore(new Date());
		perishable.setLot("lot");
		perishable = articleRepository.save(perishable);

		perishable = (Perishable) articleRepository.findOne(perishable.getId());
		assertThat(perishable.getBestBefore()).isCloseTo(Instant.now(), 1000L);
		assertThat(perishable.getLot()).isEqualTo("lot");
		articleRepository.delete(perishable);

		Product product = new Product();
		product = articleRepository.save(product);
		articleRepository.delete(product);

		Customer customer = new Customer();
		customer.setEmail("a@a");
		customer.setFirstName("firstname");
		customer.setLastName("lastname");
		customer.setLogin("login");
		customer.setPassword("BGfeiTOXva/mSnoluSgIYA==");
		customer = customerRepository.save(customer);
	}

	/**
	 * Home page: categories displayed
	 */
	@Test
	public void test2_givenDBInitialized_whenGetCategories_thenCategory() throws Exception {
		ModelAndView modelAndView = mockMvc.perform(get("/index.html"))
				.andReturn()
				.getModelAndView();

		ModelMap modelMap = modelAndView.getModelMap();
		List<Category> categories = (List<Category>) modelMap.get("categories");
		assertThat(categories).hasSize(1);
		Category category = categories.get(0);
		assertThat(category.getName()).isEqualTo("category");
		assertThat(category.getOrderIdx()).isOne();
	}

	/**
	 * Category entered: articles displayed
	 */
	@Test
	public void test3_givenDBInitialized_whenFindBy_thenCategory() throws Exception {
		Category category = categoryRepository.findAll().iterator().next();

		ModelAndView modelAndView = mockMvc.perform(get("/catalog/category/{categoryId}.html", category.getId()))
				.andReturn()
				.getModelAndView();

		ModelMap modelMap = modelAndView.getModelMap();
		List<Article> articlesAvailable = (List<Article>) modelMap.get("articles");

		assertEquals(1, articlesAvailable.size());

		Article article = articlesAvailable.get(0);
		assertThat(article.getEan()).isEqualTo("ean");
		assertThat(article.getImg()).isEqualTo("img");
		assertThat(article.getName()).isEqualTo("article");
		assertThat(article.getPrice()).isEqualTo(100);
		assertThat(article.getStock()).isOne();
		assertThat(article.getVat()).isBetween(0.1, 0.3);
	}

	@Test
	public void test4_givenNoActiveCart_whenLogin_cartCreated() throws Exception {
		Article article = articleRepository.findAll().iterator().next();
		CartPojo cartPojo = new CartPojo();
		cartPojo.setCreatedOn(Calendar.getInstance().getTime());
		cartPojo.getArticles().add(article);

		mockMvc.perform(post("/customer/login.html")
				.param("email", "a@a")
				.param("password", "a")
				.sessionAttr("target", "target")
				.sessionAttr("anonymousCart", cartPojo))
				.andReturn();

		Customer customer = customerRepository.findCustomerByEmailAndPassword("a@a", "BGfeiTOXva/mSnoluSgIYA==");
		assertThat(customer.getEmail()).isEqualTo("a@a");
		assertThat(customer.getFirstName()).isEqualTo("firstname");
		assertThat(customer.getLastName()).isEqualTo("lastname");
		assertThat(customer.getLogin()).isEqualTo("login");
		assertThat(customer.getPassword()).isEqualTo("BGfeiTOXva/mSnoluSgIYA==");

		Cart activeCart = customer.getActiveCart();
		assertThat(activeCart.getCreatedOn()).isCloseTo(cartPojo.getCreatedOn(), 1L);
		assertThat(activeCart.getCustomer()).isEqualTo(customer);
		List<Article> articles = activeCart.getArticles();
		assertThat(activeCart.getArticles()).hasSize(1);
		assertThat(articles.get(0).getId()).isEqualTo(article.getId());
	}

	@Test
	public void test5_givenCartWithOneArticle_whenAddToCart_thenCartWithTwoArticles() throws JsonProcessingException, Exception {
		Article article = articleRepository.findAll().iterator().next();
		Customer customer = customerRepository.findCustomerByEmailAndPassword("a@a", "BGfeiTOXva/mSnoluSgIYA==");

		CartEntry cartEntry = new CartEntry();
		cartEntry.setId(String.valueOf(article.getId()));
		cartEntry.setQty(1);

		mockMvc.perform(post("/cart/add.json")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(new ObjectMapper().writeValueAsString(cartEntry))
				.sessionAttr("customerId", customer.getId()))
				.andReturn();

		customer = customerRepository.findCustomerByEmailAndPassword("a@a", "BGfeiTOXva/mSnoluSgIYA==");
		Cart activeCart = customer.getActiveCart();
		List<Article> articles = activeCart.getArticles();
		assertThat(articles).hasSize(2);
	}

	@Test
	public void test6_givenCartWithTwoArticles_whenSummary_thenSummaryOK() throws Exception {
		Article article = articleRepository.findAll().iterator().next();
		Customer customer = customerRepository.findCustomerByEmailAndPassword("a@a", "BGfeiTOXva/mSnoluSgIYA==");

		mockMvc.perform(get("/cart/summary.json")
				.sessionAttr("customerId", customer.getId()))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.quantity").value("2"))
				.andExpect(jsonPath("$.amount").value(NumberFormat.getCurrencyInstance().format(2f)))
				.andExpect(jsonPath("$.entries.length()").value("1"))
				.andExpect(jsonPath("$.entries[0].id").value(article.getId()))
				.andExpect(jsonPath("$.entries[0].qty").value("2"));
	}

	@Test
	public void test7_givenCartWithTwoArticles_whenReview_thenArticlesAndAmount() throws Exception {
		Article article = articleRepository.findAll().iterator().next();
		Customer customer = customerRepository.findCustomerByEmailAndPassword("a@a", "BGfeiTOXva/mSnoluSgIYA==");

		ModelMap modelMap = mockMvc.perform(get("/cart/review.html")
				.sessionAttr("customerId", customer.getId()))
				.andExpect(status().isOk())
				.andExpect(view().name("review"))
				.andReturn()
				.getModelAndView()
				.getModelMap();

		assertThat(modelMap.get("amount")).isEqualTo(200);
		List<Article> articles = (List<Article>) modelMap.get("articles");
		assertThat(articles).hasSize(1);
		assertThat(articles.get(0).getId()).isEqualTo(article.getId());
	}

	@Test
	public void test8_givenCartWithTwoArticles_whenRemoveFromCart_thenCartWithOneArticle() throws JsonProcessingException, Exception {
		Article article = articleRepository.findAll().iterator().next();
		Customer customer = customerRepository.findCustomerByEmailAndPassword("a@a", "BGfeiTOXva/mSnoluSgIYA==");

		CartEntry cartEntry = new CartEntry();
		cartEntry.setId(String.valueOf(article.getId()));
		cartEntry.setQty(1);

		mockMvc.perform(post("/cart/remove.json")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(new ObjectMapper().writeValueAsString(cartEntry))
				.sessionAttr("customerId", customer.getId()))
				.andReturn();

		customer = customerRepository.findCustomerByEmailAndPassword("a@a", "BGfeiTOXva/mSnoluSgIYA==");
		Cart activeCart = customer.getActiveCart();
		List<Article> articles = activeCart.getArticles();
		assertThat(articles).hasSize(1);
	}

	@Test
	public void test9_givenCartWithOneArticle_whenValidateCart_thenOrderCreated() throws Exception {
		Customer customer = customerRepository.findCustomerByEmailAndPassword("a@a", "BGfeiTOXva/mSnoluSgIYA==");

		mockMvc.perform(get("/cart/validate.html")
				.sessionAttr("customerId", customer.getId()))
				.andExpect(status().is3xxRedirection())
				.andExpect(redirectedUrl("/customer/orders.html"))
				.andExpect(flash().attribute("confirmation", "Votre commande a bien été validée"));

		customer = customerRepository.findCustomerByEmailAndPassword("a@a", "BGfeiTOXva/mSnoluSgIYA==");
		Cart activeCart = customer.getActiveCart();
		List<Article> articles = activeCart.getArticles();
		assertThat(articles).isEmpty();

		List<Order> orders = orderRepository.findByCustomerOrderByCreatedOnDesc(customer);
		assertThat(orders).hasSize(1);
		Order order = orders.get(0);
		assertThat(order.getAmount()).isEqualTo(100);
		assertThat(order.getCurrentStatus()).isEqualTo(Status.ORDERED);
		assertThat(order.getCustomer().getId()).isEqualTo(customer.getId());
		assertThat(order.getDeliveryDate()).isAfter(Instant.now());
		assertThat(order.getCreatedOn()).isCloseTo(Instant.now(), 1000L);

		assertThat(statusHistoryRepository.count()).isEqualTo(1L);
		StatusHistory statusHistory = statusHistoryRepository.findAll().iterator().next();
		assertThat(statusHistory.getStatus()).isEqualTo(Status.ORDERED);
		assertThat(statusHistory.getStatusDate()).isCloseTo(Instant.now(), 1000L);

		Order orderWithArticles = orderRepository.findOne(order.getId());
		assertThat(orderWithArticles.getArticles()).hasSize(1);
	}

}
