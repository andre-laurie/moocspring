package fr.eservices.drive.web;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import fr.eservices.drive.config.AppConfig;
import fr.eservices.drive.config.ServiceConfig;
import fr.eservices.drive.config.WebConfig;

@WebAppConfiguration
@ContextConfiguration(classes = { WebConfig.class, ServiceConfig.class, AppConfig.class, TestDataConfig.class })
@Target(TYPE)
@Retention(RUNTIME)
public @interface IntegrationTestConfiguration {

}
