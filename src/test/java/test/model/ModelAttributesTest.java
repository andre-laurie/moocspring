package test.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import fr.eservices.drive.model.Article;
import fr.eservices.drive.model.Cart;
import fr.eservices.drive.model.Category;
import fr.eservices.drive.model.Customer;
import fr.eservices.drive.model.Order;
import fr.eservices.drive.model.Perishable;
import fr.eservices.drive.model.Product;
import fr.eservices.drive.model.Status;
import fr.eservices.drive.model.StatusHistory;

/**
 * Conformance check to Week3 model
 */
public class ModelAttributesTest {

	@Test
	public void testCategory() {
		Class<?> k = Category.class;
		assertHasField(k, "name", String.class);
		assertHasField(k, "orderIdx", int.class);
	}

	@Test
	public void testArticle() {
		Class<?> k = Article.class;
		assertHasField(k, "ean", String.class);
		assertHasField(k, "price", int.class);
		assertHasField(k, "vat", double.class);
		assertHasField(k, "name", String.class);
		assertHasField(k, "img", String.class);
		assertHasField(k, "categories", List.class);
	}

	@Test
	public void testPerishable() {
		Class<?> k = Perishable.class;
		assertHasField(k, "bestBefore", Date.class);
		assertHasField(k, "lot", String.class);
		assertTrue(Article.class.isAssignableFrom(k));
	}

	@Test
	public void testProduct() {
		assertTrue(Article.class.isAssignableFrom(Product.class));
	}

	@Test
	public void testCart() {
		Class<?> k = Cart.class;
		assertHasField(k, "createdOn", Date.class);
		assertHasField(k, "articles", List.class);
		assertHasField(k, "customer", Customer.class);
	}

	@Test
	public void testCustomer() {
		Class<?> k = Customer.class;
		assertHasField(k, "login", String.class);
		assertHasField(k, "firstName", String.class);
		assertHasField(k, "lastName", String.class);
		assertHasField(k, "email", String.class);
		assertHasField(k, "password", String.class);
		assertHasField(k, "activeCart", Cart.class);
	}

	@Test
	public void testOrder() {
		Class<?> k = Order.class;
		assertHasField(k, "createdOn", Date.class);
		assertHasField(k, "deliveryDate", Date.class);
		assertHasField(k, "amount", int.class);
		assertHasField(k, "articles", List.class);
		assertHasField(k, "history", List.class);
		assertHasField(k, "customer", Customer.class);
		assertHasField(k, "currentStatus", Status.class);
	}

	@Test
	public void testStatusHistory() {
		Class<?> k = StatusHistory.class;
		assertHasField(k, "statusDate", Date.class);
		assertHasField(k, "status", Status.class);
	}

	private void assertHasField(Class<?> clazz, String name, Class<?> fieldType) {
		try {
			Field field = clazz.getDeclaredField(name);
			assertEquals(fieldType, field.getType());
		} catch (NoSuchFieldException e) {
			fail("Class " + clazz.getName() + " must have field " + name);
		} catch (SecurityException e) {
			throw new IllegalStateException("Error accessing fields");
		}
	}

}
